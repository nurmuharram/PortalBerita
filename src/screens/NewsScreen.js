import React from 'react';
import {View, Text, StyleSheet,ScrollView,Image} from 'react-native';
import NewsItem from '../components/NewsItem.component';
import axios from 'axios';

export default class app extends React.Component {
   constructor (props){
     super(props);
     this.state={
       listNews:[],
     };
   }
   componentDidMount(){
     axios({
       method : "GET",
       url : "http://newsapi.org/v2/everything?q=bitcoin&from=2020-09-20&sortBy=publishedAt&apiKey=2ba737ac7f9b4e5aa2df4eeeb7730895",
     })
     .then((res)=>{
       this.setState({
        listNews:res.data.articles, 
       });
     })
        .catch((err)=>{

        });

   }
    render(){
     return(
       <View>
        <View>
          <Text style={styles.header}>NEWS FOR YOU</Text>
        </View>
      
        <ScrollView>
          {this.state.listNews.map((news, index) => (
            <NewsItem
            key={index}
            title={news.title}
            author={news.author}
            image={news.urlToImage}
            description={news.description}
            date={news.publishedAt}
            />
            ))}
         </ScrollView>
      </View> 
     );
    }

}

const styles = StyleSheet.create({
  header: {
    fontWeight: 'bold',
    textAlign: 'center',
  },
});
