import React from 'react';
import {View, Image, Text, StyleSheet} from 'react-native';

export default class NewsItem extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={styles.itemContainer}>
        <View style={styles.contImage}>
          <Image source={{uri:this.props.image}}style={styles.itemImage} />
        </View>

        <View style={styles.itemContent}>
          <View>
            <Text> {this.props.title}</Text>
          </View>
          <View>
            <Text style={styles.itemText}> {this.props.name}{this.props.date}</Text>
          </View>  
            
          
          <View><Text style={styles.itemText}>{this.description}</Text></View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  itemContainer:{
   display : "flex",
   flexDirection : "row",
   padding : 10, 
  },
  contImage:{
    flex : 2,
    alignItems : "center",
    justifyContent : "center",
  },
  itemImage:{
    width : 100,
    height : 100,
  },
  itemContent:{
    flex : 3,

  },
  itemText:{
    display : "flex",
    flexDirection : "row",

  },
  title:{
    fontWeight : "bold",
    
  }

});
