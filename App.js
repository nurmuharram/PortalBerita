import React from 'react';
import {SafeAreaView, StatusBar} from 'react-native';
import NewsScreen from './src/screens/NewsScreen';

function App() {
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <NewsScreen />
      </SafeAreaView>
    </>
  );
}

export default App;
